from django.contrib import admin

from productos.models import Producto, ImagenesProducto

admin.site.register(Producto)
admin.site.register(ImagenesProducto)
