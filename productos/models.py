from django.db import models


class ImagenesProducto(models.Model):
    imagen = models.ImageField(blank=True, null=True)


class Producto(models.Model):
    nombre = models.CharField(max_length=250)
    imagen = models.ImageField()
    descripcion = models.CharField(max_length=250, blank=True)
    precio = models.DecimalField(max_digits=5, decimal_places=2, default=0)  # 999.00
    activo = models.BooleanField(default=False)
    galeria = models.ManyToManyField(ImagenesProducto)

    def __str__(self):
        return self.nombre
